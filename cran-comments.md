## Update 2024-04-07

* updated documentation
* fixed unit test to resolve dependency error

### Test environments
* local R installation, Ubuntu 22.04 R 4.3.3
* win-builder (release, devel, oldrelease)
* rhub (windows, macOS-highsierra, macOS-bigsur, fedora, ubuntu, debian-clang-devel)

### Local R CMD check results

0 errors  | 0 warnings  | 0 notes



# Previous Cran Comments

## Update 2024-03-11

* added functions / unit tests

### Test environments
* local R installation, Ubuntu 22.04 R 4.3.3
* win-builder (release, devel, oldrelease)
* rhub (windows, macOS-highsierra, macOS-bigsur, fedora, ubuntu, debian-clang-devel)

### Local R CMD check results

0 errors  | 0 warnings  | 0 notes


## Resubmission 2024-10-13

- fix doi typo
- set default cores to 2

### Test environments
* local R installation, Ubuntu 22.04 R 4.3.2
* win-builder (release, devel, oldrelease)
* rhub (windows, macOS-highsierra, macOS-bigsur, fedora, ubuntu, debian-clang-devel)

### Local R CMD check results

0 errors  | 0 warnings  | 0 notes



## Update 2023-12-27

- Fixed additional (MKL) issues (test failing on CRAN because of a dependency)

### Test environments
* local R installation, Ubuntu 22.04 R 4.3.2
* win-builder (release, devel, oldrelease)
* rhub (windows, macOS-highsierra, macOS-bigsur, fedora, ubuntu, debian-clang-devel)

### Local R CMD check results

0 errors  | 0 warnings  | 0 notes 

## Update 2023-06-16

- Fixed URLs
- Fixed maintainer email address
- Fixed additional (MKL) issues (test failing on CRAN because of a dependency)

### Test environments
* local R installation, Ubuntu 22.04 R 4.3.0
* win-builder (release, devel, oldrelease)
* rhub (windows, macOS-highsierra, macOS-bigsur, fedora, ubuntu, debian-clang-devel)

### Local R CMD check results

0 errors  | 0 warnings  | 0 notes 

Note: some platforms give erroneous 403 error for https://opensource.org/license/mit/


## Update 2023-06-16

Changed maintainer email address

Fix encoding issue for non-ASCII characters to work with `fastmatch`
Add functionality 
- `perm_tester` for Monte Carlo Permutation Tests for Model P-Values
- `rancor_builder` creates random corpus based on provided term probabilities 
- `rancors_builder` creates multiple random corpora

Include a few additional tests, updated documentation, and vignettes

### Test environments
* local R installation, ubuntu 22.04 R 4.2.1
* local macOS Big Sur 11.6.5, R 4.1.1
* win-builder (release, devel, oldrelease)
* rhub (windows, macOS-highsierra, macOS-bigsur, fedora, ubuntu, debian-clang-devel)

### R CMD check results

0 errors  | 0 warnings  | 0 notes 


## Update 2022-08-17

This update fixes a broken URL.

### Test environments
* local R installation, ubuntu 22.04 R 4.2.1
* local macOS Big Sur 11.6.5, R 4.1.1
* win-builder (release, devel, oldrelease)
* rhub (windows, macOS-highsierra, macOS-bigsur, fedora, ubuntu, debian-clang-devel)

### R CMD check results

0 errors  | 0 warnings  | 0 notes 


## Update 2022-08-15

This is an update. Fix Rd HTML issues and added two function.

### Test environments
* local R installation, ubuntu 22.04 R 4.2.1
* local macOS Big Sur 11.6.5, R 4.1.1.
* win-builder (release, devel, oldrelease)
* rhub (windows, macOS-highsierra, macOS-bigsur, fedora, ubuntu, debian-clang-devel)

### R CMD check results

0 errors  | 0 warnings  | 0 notes 

## Update 2022-05-21

This is an update. Replaced a dependency, updated methods, and added a function.

### Test environments
* local R installation, ubuntu 22.04 R 4.2.0
* local macOS 11.3.1, R 4.2.0
* ubuntu 16.04 (on travis-ci), R 4.2.0
* win-builder (release, devel, oldrelease)
* rhub (windows, macOS-highsierra, macOS-bigsur, fedora, ubuntu, debian-clang-devel)

### R CMD check results

0 errors  | 0 warnings  | 0 notes 

### Downstream dependencies
There are currently no downstream dependencies for this package


## Update 2022-04-08

This is an update. We added arguments for existing functions, fixed warnings/messages, added tests, updated documentation.

### Test environments
* local R installation, ubuntu 21.10 R 4.1.3
* local macOS 11.3.1, R 4.1.0
* ubuntu 16.04 (on travis-ci), R 4.1.0
* win-builder (release, devel)
* rhub (windows, macOS-highsierra, macOS-bigsur, fedora, ubuntu, debian-clang-devel)

### RMD Check Results

0 errors  | 0 warnings  | 0 notes 

## Update

This is a update. We have added new functions: 
 - find_transformation
 - find_projection
 - find_rejection
 - dtm_melter

We have also improved functionality and documentation for current functions

### Test environments
* local R installation, ubuntu 21.04 R 4.1.0
* local macOS 11.3.1, R 4.1.0
* ubuntu 16.04 (on travis-ci), R 4.1.0
* win-builder (release, devel)
* rhub (windows, macOS-highsierra, macOS-bigsur, fedora, ubuntu, debian-clang-devel)

### R CMD check results

0 errors  | 0 warnings  | 0 notes 

### Downstream dependencies
There are currently no downstream dependencies for this package

## Resubmission

This is a resubmission. We have fixed the build error on debian-clang-devel

## Test environments
* local R installation, ubuntu 21.04 R 4.1.0
* local macOS 11.3.1, R 4.1.0
* ubuntu 16.04 (on travis-ci), R 4.1.0
* win-builder (release, devel)
* rhub (windows, macOS-highsierra, macOS-bigsur, fedora, ubuntu, debian-clang-devel)

## R CMD check results

0 errors  | 0 warnings  | 0 notes 

## Downstream dependencies
There are currently no downstream dependencies for this package


## Test environments
* local R installation, ubuntu 21.04 R 4.1.0
* local macOS 11.3.1, R 4.1.0
* ubuntu 16.04 (on travis-ci), R 4.1.0
* win-builder (release, devel)
* rhub (windows, fedora, ubuntu)

## R CMD check results

0 errors | 0 warnings | 1 note

Only NOTE: "Possibly mis-spelled words in DESCRIPTION" is false positive

* This is a new release.

## Downstream dependencies
There are currently no downstream dependencies for this package

## Resubmission

This is a second resubmission of a new package, responding to 
feedback from Julia Haider. In this version we have:

* Added references describing the methods to the DESCRIPTION
* Added single quotes to package names (i.e. 'Matrix')
* Added \value to four .Rd files that were missing this field
* Removed unnecessary \dontrun{}
* Checked no packages were installed in examples or vignettes
* Checked no more than 2 cores are used in examples or vignettes
