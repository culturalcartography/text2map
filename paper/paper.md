---
title: 'text2map: R Tools for Text Matrices'
tags:
- R
- text analysis
- word embeddings
- document-term matrices
date: "25 August 2021"
affiliations:
- name: Lehigh University
  index: 1
- name: New Mexico State University
  index: 2
authors:
- name: Dustin S. Stoltz^[co-first author]
  orcid: 0000-0002-4774-0765
  affiliation: 1
- name: Marshall A. Taylor^[co-first author]
  orcid: 0000-0002-7440-0723
  affiliation: 2
bibliography: paper.bib
output:
  html_document:
    keep_md: yes
---

# Summary

`text2map` is an `R` [@R] package that provides several tools for working with text matrices, including document-term matrices, term-context matrices, and word embedding matrices. `text2map` is published at The Comprehensive R Archive Network (CRAN) at [https://cran.r-project.org/package=text2map](https://cran.r-project.org/package=text2map); its source is available at [https://gitlab.com/culturalcartography/text2map/](https://gitlab.com/culturalcartography/text2map/). `text2map` contains vignettes demonstrating basic functionality and more advanced uses of the package. The website, which includes function documentation, is available at [https://culturalcartography.gitlab.io/text2map/](https://culturalcartography.gitlab.io/text2map/).

Specifically, `text2map` offers functions for creating semantic centroids, semantic regions, semantic directions, semantic projections, transforming embeddings, and performing concept mover's distance and concept class analysis [@Arseniev-Koehler2021-pg; @Arseniev-Koehler2020-vc; @Boutyline2020-su; @Nelson2021-zo; @Jones2020-hp; @Carbone2022-ko; @Stoltz2019-ca; @Stoltz2021-cc; @Taylor2020-yc; @Taylor2020-mf; @Caliskan2017-et].

Embeddings often need to be transformed after training. `find_transformation` provides methods for normalizing and centering large embedding matrices. Embedding matrices which have been trained separately can also be "aligned" using `find_transformation` using the Procrustes method. Embeddings can also be "projected" onto a given vector or "rejected" from a given vector using `find_projection` and `find_rejection` respectively.

**Semantic centroids** are increasingly used to "fine-tune" pretrained embedding vectors. `text2map` takes a vector of "anchor" terms and finds the vector average for those terms. This allows the user to specify a given sense of a word or concept. Similarly, **semantic directions** are used to define a one-dimensional subspace corresponding to a bipolar concept -- such as "big" to "small" [@Grand2022-es] or "conservative" to "liberal" [@Taylor2020-mf]. The most well known example is "gender," where masculine terms anchor one side of the direction and feminine terms anchor the other. `text2map` offers four methods for defining a semantic direction: paired (each individual term is subtracted from exactly one other paired term), pooled (terms corresponding to one side of a direction are first averaged, and then these averaged vectors are subtracted), L2 (the direction is calculated the same as with "pooled" but is then divided by the L2 Euclidean norm), and PCA (vector offsets are calculated for each pair of terms, as with "paired," and if `n_dirs = 1L` (the default) then the direction is the first principal component). With the latter option, users can return more than one direction. `text2map` also provides a range of precompiled anchor lists from published works, defining 26 semantic relations, including gender and status. We should emphasis that these should be used as a starting point, and not as "ground truth."

In addition, `text2map` provides tools related to the document-term matrix (DTM), including a fast unigram DTM builder and comprehensive DTM "stopping" function, both of which are built around the Matrix class `dgCmatrix`. The package's DTM resampler function takes any DTM and randomly resamples from each row, creating a new DTM randomly. This can be useful in many downstream tasks. 

# Statement of Need

`text2map` offers a consistent set of tools built around representing texts as matrices. This is in contrast to `corpus` objects [@Perry2021] or `tidytext`'s triplet data frame [@Silge2016]. This allows `text2map` to remain close to the underlying matrix mathematics of contemporary computational text analysis as well as make use of memory-efficient matrix packages---e.g., `Matrix` [@Bates2010-bt]. `text2map` also avoids special-purpose classes, such as `quanteda`'s `dfm` class. While there are `R` packages for training word embeddings---e.g.,`text2vec` [@Selivanov2020-qk]---none offer methods for working with embeddings in downstream tasks, in particular, tasks involved in social scientific and digital humanities research. For example, to the best of our knowledge, `text2map` is the only `R` package that provides functions dedicated to finding semantic centroids, semantic directions, and embedding matrix transformations. As such, packages such as `text2vec` can be used to train embeddings, and `text2map` can then be used to ready the embeddings for downstream analyses. Further, while `quanteda` is a general purpose suite of tools built around the corpus object, `text2map` focuses on the matrix as the central data object.

# Illustration

Let's consider a simple example of `text2map` in use in a humanities context. Building off some of our previous work [@Stoltz2019-ca; @Taylor2020-mf], say a researcher is interested in examining the extent to which Shakespeare's First Folio plays engage the concept of "death." 

The `text2map` package can be used to efficiently convert the raw corpus of plays into a document-term matrix (DTM), compute several different types of summary statistics on that DTM, and then measure each plays engagement with the concept of interest. The package will also work with DTM built from other popular text analysis packages.




Using `dtm_stats` we can get a series of summary statistics. The table below illustrates potential output (hapax, dis, tris refers to terms occurring just once, twice, and thrice, respectively, in the corpus).

|   |Measure        |Value  |
|:--|:--------------|:------|
|1  |Total Docs     |37     |
|2  |Percent Sparse |87.70% |
|3  |Total Types    |29957  |
|4  |Total Tokens   |889428 |
|5  |Object Size    |3.6 Mb |
Table: Basic DTM Information

|   |Measure          |Value  |
|:--|:----------------|:------|
|1  |Percent Hapax    |47.00% |
|2  |Percent Dis      |25.00% |
|3  |Percent Tris     |21.00% |
|4  |Type-Token Ratio |0.03   |
Table: Lexical Richness Metrics

We can then quickly generate concept mover's distance (CMD) scores using the `CMDist` function and a matrix of word vectors. The user can also add "sensitivity intervals" shown in the plot using the package's `dtm_resampler` function to assess how robust each production's CMD score is (or is not) to the specific vocabulary frequency distribution of that document.

In the example, the CMD of each play to the single word "death" is computed, however, the user could use `get_centroid` to specify death using a few synonyms. We could also compute each play's CMD to death *as opposed to* life by using the output of `get_direction` and pairs of anchors for life and death, respectively.

![Illustrative figure. Scatterplot of Shakespeare play's CMD scores (*y*-axis) and the body count in the narrative (*x*-axis). Bands are sensitivity intervals, which are the CMD scores at the 2.5 and 97.5 percentiles for each document after resampling the vocabulary from the document 20 times.](figure1.png)

# Acknowledgements

We would like to thank Michael Lee Wood for helpful advice and test-runs with the software. We would also like to thank Brandon Sepulvado for his assistance in fixing a parallelization error in earlier iterations of the `CMDist` code.

# References
